FROM golang:1.12.0-stretch

WORKDIR go/
COPY . /go

CMD ["g", "rn", "mai.go"]